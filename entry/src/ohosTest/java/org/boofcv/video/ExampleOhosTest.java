package org.boofcv.video;

import boofcv.struct.image.GrayU8;
import boofcv.ohos.BOverrideConvertOhos;
import boofcv.ohos.ConvertBitmap;
import boofcv.ohos.ImplConvertBitmap;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.ddogleg.struct.DogArray_I8;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("org.boofcv.video", actualBundleName);
    }

    @Test
    public void testBitmap() {
        PixelMap pixelMap = PixelMap.create(ConvertBitmap.createOpts(1, 1, PixelFormat.ARGB_8888));
        DogArray_I8 dogArray_i8 = ConvertBitmap.resizeStorage(pixelMap, new DogArray_I8());
        Assert.assertNotNull(dogArray_i8);
    }

    @Test
    public void testConvert() {
        PixelMap pixelMap = PixelMap.create(ConvertBitmap.createOpts(1, 1, PixelFormat.ARGB_8888));
        boolean isOk = BOverrideConvertOhos.invokeBitmapToBoof(pixelMap,
                new GrayU8(1, 1), new byte[100]);
        Assert.assertNotNull(isOk);
    }
    @Test
    public void testImplConvert() {
        PixelMap pixelMap = PixelMap.create(ConvertBitmap.createOpts(1, 1, PixelFormat.ARGB_8888));
        GrayU8 grayU8 = new GrayU8(1, 1);
        ImplConvertBitmap.arrayToGray(new byte[100],
                pixelMap.getImageInfo().pixelFormat, grayU8);
        Assert.assertNotNull(grayU8);
    }
}