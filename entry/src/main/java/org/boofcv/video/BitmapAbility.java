package org.boofcv.video;

import org.boofcv.video.slice.BitmapSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class BitmapAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BitmapSlice.class.getName());
    }
}