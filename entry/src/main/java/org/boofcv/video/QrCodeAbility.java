package org.boofcv.video;

import org.boofcv.video.slice.QrCodeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class QrCodeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(QrCodeSlice.class.getName());
    }
}