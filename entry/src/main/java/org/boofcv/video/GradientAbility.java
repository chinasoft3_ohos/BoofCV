package org.boofcv.video;

import org.boofcv.video.slice.GradientSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class GradientAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GradientSlice.class.getName());
    }
}