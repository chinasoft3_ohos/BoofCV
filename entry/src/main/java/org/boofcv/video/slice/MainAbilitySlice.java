package org.boofcv.video.slice;

import org.boofcv.video.BitmapAbility;
import org.boofcv.video.ExposureAbility;
import org.boofcv.video.GradientAbility;
import org.boofcv.video.QrCodeAbility;
import org.boofcv.video.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.security.SystemPermission;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    public static final int EVENT_PERMISSION_GRANTED = 0x0000023;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initListener();
        requestCameraPermission();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void initListener() {
        findComponentById(ResourceTable.Id_btn_gradient).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_exposure).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_qrcode).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_open_image).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_gradient:
                starAbility(GradientAbility.class);
                break;
            case ResourceTable.Id_btn_exposure:
                starAbility(ExposureAbility.class);
                break;
            case ResourceTable.Id_btn_qrcode:
                starAbility(QrCodeAbility.class);
                break;
            case ResourceTable.Id_btn_open_image:
                starAbility(BitmapAbility.class);
                break;
        }
    }

    private void requestCameraPermission() {
        List<String> permissions =
                new LinkedList<String>(
                        Arrays.asList(
                                SystemPermission.WRITE_USER_STORAGE,
                                SystemPermission.READ_USER_STORAGE,
                                SystemPermission.CAMERA,
                                SystemPermission.MICROPHONE));
        permissions.removeIf(
                permission ->
                        verifySelfPermission(permission) == PERMISSION_GRANTED || !canRequestPermission(permission));

        if (!permissions.isEmpty()) {
            requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]), 1);
        } else {
            new EventHandler(EventRunner.current()).sendEvent(EVENT_PERMISSION_GRANTED);
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void starAbility(Class<?> cls) {
        Intent intent = new Intent();
        intent.setOperation(new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(cls)
                .build());
        startAbility(intent);
    }
}
