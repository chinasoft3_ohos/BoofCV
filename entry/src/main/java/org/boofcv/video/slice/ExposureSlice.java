package org.boofcv.video.slice;

import boofcv.ohos.camera.SimpleCameraSlice;
import org.boofcv.video.ResourceTable;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Rect;
import ohos.media.camera.device.Camera;
import ohos.media.camera.device.CameraAbility;
import ohos.media.camera.device.FrameConfig;
import ohos.media.camera.params.Metadata;
import ohos.media.image.Image;

import java.util.Locale;

public class ExposureSlice extends SimpleCameraSlice {
    public static final String TAG = "Exposure";
    // The application forces there to be at most 7 selectable levels to make this more hardware
    // independent.
    int level = 3, numLevels = 7;
    int minEV, maxEV;

    // Displays exposure information
    Text textShow;
    // Time of last exposure change to prevent spamming
    long timeLastClick = 0;

    public ExposureSlice() {
//        setVisible(true);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_exposure);

        // This will steam the feed to a texture view. No image
        // processing or bitmaps required
        textShow = (Text) findComponentById(ResourceTable.Id_text_show);

        StackLayout stackLayout = (StackLayout) findComponentById(ResourceTable.Id_sl_camera_view_show);
        stackLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // Cool down to prevent it from trying to change too quickly
                if (System.currentTimeMillis() < timeLastClick)
                    return;
                timeLastClick = System.currentTimeMillis() + 250;

                // Cycle through all possible values
                level = (level + 1) % numLevels;
                int ev = computeEV();
                changeCameraConfiguration();

                // Display info to the user
                String message = String.format(Locale.getDefault(), "min=%3d max=%3d current=%3d", minEV, maxEV, ev);
                textShow.setText(message);
            }
        });

        startCameraTexture(stackLayout);
    }

    @Override
    protected void configureCamera(Camera device, CameraAbility characteristics, FrameConfig.Builder captureRequestBuilder) {
        captureRequestBuilder.setAfMode(Metadata.AfMode.AF_MODE_CONTINUOUS, null);
        captureRequestBuilder.setAeMode(Metadata.AeMode.AE_MODE_ON, new Rect(0, 0, minEV, maxEV));
        // Divide the total exposure compensation needed by the size of the exposure compensation steps to be able to pass in the number of steps
//        Range<Integer> range = characteristics.get(CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE);
//        if (range != null) {
//            minEV = range.getLower();
//            maxEV = range.getUpper();
//            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, computeEV());
//        } else {
//            Toast.makeText(this, "Can't adjust camera's EV", Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    protected void processFrame(Image image, int width, int height) {
        // Add image processing here
    }

    private int computeEV() {
        return (int) Math.round((maxEV - minEV) * (level / (double) (numLevels - 1))) + minEV;
    }
}
