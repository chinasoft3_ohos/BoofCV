﻿# BoofCV

#### 项目介绍
- 项目名称：BoofCV
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个开源实时计算机视觉库，功能包括低层图像处理、摄像机标定、特征检测/跟踪、运动结构、分类和识别。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v0.37

#### 效果演示
![image text](gif/demo1.gif)

由于部分平台限制，未登录状态只能看到1M以下的图，如需要查看全部效果，请登录后查看。

![image text](gif/demo2.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:BoofCV:1.0.0')
   ......  
}
```
#### 使用说明
- 方法一
直接使用库中的静态方法进行图像处理。

- 方法二
继承SimpleCameraSlice，并且调用父方法startCameraTexture启动摄像头的预览。
在通过重写configureCamera，processFrame等方法，进行摄像头设置。

- 方法三
继承VisualizeCameraSlice，并且调用父方法startCamera启动摄像头的预览。
在通过重写configureCamera，processFrame，onDrawFrame等方法，进行摄像头和预览画面进行设置


#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0