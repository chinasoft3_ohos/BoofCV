/*
 * Copyright (c) 2011-2020, Peter Abeles. All Rights Reserved.
 *
 * This file is part of BoofCV (http://boofcv.org).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boofcv.ohos;

import boofcv.alg.color.ColorFormat;
import boofcv.override.BOverrideClass;
import boofcv.override.BOverrideManager;
import boofcv.struct.image.ImageBase;
import ohos.media.image.Image;
import ohos.media.image.PixelMap;

/**
 * Generalized operations related to compute different image derivatives.
 *
 * @author:Peter Abeles
 * @since 2021-05-11
 */
public class BOverrideConvertOhos extends BOverrideClass {
    static {
        BOverrideManager.register(BOverrideConvertOhos.class);
    }

    public static final YuvToBoof_420888 yuv420ToBoof = null;
    public static final BitmapToBoof bitmapToBoof = null;
    public static final BoofToBitmap boofToBitmap = null;

    public interface YuvToBoof_420888<T extends ImageBase<T>> {
        void yuvToBoof420(Image input, ColorFormat color, ImageBase output);
    }

    public interface BitmapToBoof<T extends ImageBase<T>> {
        void bitmapToBoof(PixelMap input, ImageBase output, byte[] work);
    }

    public interface BoofToBitmap<T extends ImageBase<T>> {
        void boofToBitmap(ColorFormat color, ImageBase input, PixelMap output, byte[] work);
    }

    public static boolean invokeYuv420ToBoof(Image input, ColorFormat color, ImageBase output) {
        boolean processed = false;
        if (BOverrideConvertOhos.yuv420ToBoof != null) {
            try {
                BOverrideConvertOhos.yuv420ToBoof.yuvToBoof420(input, color, output);
                processed = true;
            } catch (RuntimeException ignore) {
            }
        }
        return processed;
    }

    public static boolean invokeBitmapToBoof(PixelMap input, ImageBase output, byte[] work) {
        boolean processed = false;
        if (BOverrideConvertOhos.bitmapToBoof != null) {
            try {
                BOverrideConvertOhos.bitmapToBoof.bitmapToBoof(input, output, work);
                processed = true;
            } catch (RuntimeException ignore) {
            }
        }
        return processed;
    }

    public static boolean invokeBoofToBitmap(ColorFormat color, ImageBase input, PixelMap output, byte[] work) {
        boolean processed = false;
        if (BOverrideConvertOhos.boofToBitmap != null) {
            try {
                BOverrideConvertOhos.boofToBitmap.boofToBitmap(color, input, output, work);
                processed = true;
            } catch (RuntimeException ignore) {
            }
        }
        return processed;
    }
}
