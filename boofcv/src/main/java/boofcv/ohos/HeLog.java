/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boofcv.ohos;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 日志打印类
 *
 * @author:zhaojun
 * @since 2021-05-11
 */
public final class HeLog {
    private static final String LOG_FORMAT = "%{public}s:%{public}s";
    private static final String TAG = "HeLog";
    private static final int DOMAIN = 0xD000f00;

    private HeLog() {
    }

    /**
     * 打印debug日志
     *
     * @param tag
     * @param info
     */
    public static void d(String tag, String info) {
        HiLog.debug(new HiLogLabel(HiLog.DEBUG, DOMAIN, tag), LOG_FORMAT, info);
    }

    /**
     * 打印debug日志
     *
     * @param info
     */
    public static void d(String info) {
        d(TAG, info);
    }

    /**
     * 打印debug日志
     *
     * @param info
     */
    public static void d(int info) {
        d(TAG, info + "");
    }

    /**
     * 打印info日志
     *
     * @param tag
     * @param info
     */
    public static void i(String tag, String info) {
        HiLog.info(new HiLogLabel(HiLog.INFO, DOMAIN, tag), LOG_FORMAT, info);
    }

    /**
     * 打印info日志
     *
     * @param info
     */
    public static void i(String info) {
        i(TAG, info);
    }

    /**
     * 打印info日志
     *
     * @param info
     */
    public static void i(int info) {
        i(TAG, info + "");
    }

    /**
     * 打印error日志
     *
     * @param tag
     * @param info
     */
    public static void e(String tag, String info) {
        HiLog.error(new HiLogLabel(HiLog.ERROR, DOMAIN, tag), LOG_FORMAT, info);
    }

    /**
     * 打印error日志
     *
     * @param info
     */
    public static void e(String info) {
        e(TAG, info);
    }

    /**
     * 打印error日志
     *
     * @param info
     */
    public static void e(int info) {
        e(TAG, info + "");
    }
}
